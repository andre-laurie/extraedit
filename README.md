# Extra Edit

## Description

Extra Edit is an Eclipse plugin to provide a few extra features:

### SelectCurrentIdentifier

This command expands the selection to enclose the current identifier. It's based on string rules and editor contents. Eclipse offers a builtin equivalent, but it is reliable only when the compilation unit is syntactically correct. In other words, it is reliable when we're not editing the file, and have little or no need for selecting whole identifiers.

### Case handling

Upper/Lower/Title case commands act on the word under the caret when the selection is empty, alleviating from the need to first select the word.

### PropertyBeanCase

This command transforms the getter under the carret to its property bean name:
`getFooBar` is transformed to `fooBar` and `isFooBar` is transformed to `fooBar`

### ExpandQuote (WIP)

This command expands the selection to, but not enclosing it, the next quotation marks (" or """). A subsequent call encloses the delimiters.

Thoughts: this command could be unified to usual pairing brackets '()', '{}', '[]', '<>'.
I have found no simple mouseless builtin way to select all parameters, a block, generics without the delimiters.

A call expands to but excludes delimiters.
Next call encloses the delimiters.
Next call encloses to the next delimiters.

### Increment / decrement (WIP)

Increments / decrements number or number suffixed word under caret.

123 -> increment -> 124
Foo1 -> increment -> Foo2 (Handy with made up data in unit tests).

## Building

Manual building, no headless build for update site.
Manually run JUnit Tests.

## Requirements

Written in legacy Eclipse 3.
Source code in Java17.


## Contributing

I guess although this project is public, no one will ever stumble upon it. 
Just in case, feel free to fork it or submit a MR.

## Licensing

Copyright 2024 André Laurie

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

3rd Party Software Included
Apache commons Lang - Apache License 2.0
Apache commons Text - Apache License 2.0
