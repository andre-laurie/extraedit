package com.alaurie.text;

import java.util.stream.IntStream;

public record Region(int offset, int length) {
	
	public IntStream stream() {
		return IntStream.range(offset, offset+length);
	}
	
	public boolean isEmpty() {
		return length == 0;
	}

}
