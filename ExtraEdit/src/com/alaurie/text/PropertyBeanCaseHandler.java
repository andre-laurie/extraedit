package com.alaurie.text;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.texteditor.ITextEditor;

public class PropertyBeanCaseHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IEditorPart activeEditorChecked = HandlerUtil.getActiveEditorChecked(event);

		if (activeEditorChecked instanceof ITextEditor textEditor) {
			SelectionHelper selectionHelper = SelectionHelper.of(textEditor);

			Region selection = selectionHelper.currentSelection();

			Predicate<Character> predicate = Character::isJavaIdentifierPart;

			Region workingRegion = Optional.of(selection).filter(Region::isEmpty)
					.map(region -> selectionHelper.expand(region, predicate)).orElse(selection);

			String text = selectionHelper.getText(workingRegion);

			String propertyBean = List.of("is", "get").stream()
					.map(prefix -> (Function<String, String>) t -> StringUtils.removeStart(t, prefix))
					.reduce(Function.identity(), Function::compose).apply(text);

			Runnable runnable;
			int deltaLength = propertyBean.length() - text.length();
			if (selection.isEmpty()) {
				Control beforeFocusControl = Display.getCurrent().getFocusControl();
				if (beforeFocusControl instanceof StyledText styledText) {
					int caretOffset = styledText.getCaretOffset() + deltaLength;
					runnable = () -> styledText.setCaretOffset(caretOffset);
				} else
					runnable = () -> {
					};
			} else {
				runnable = () -> textEditor.getSelectionProvider()
						.setSelection(new TextSelection(selection.offset(), selection.length() + deltaLength));
			}

			selectionHelper.replace(workingRegion, propertyBean, runnable);

		}

		return null;
	}
}