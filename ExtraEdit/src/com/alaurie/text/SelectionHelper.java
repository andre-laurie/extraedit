package com.alaurie.text;

import java.util.function.Predicate;
import java.util.stream.Collector;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

public class SelectionHelper {
	

	private ITextEditor textEditor;

	private SelectionHelper(ITextEditor textEditor) {
		this.textEditor = textEditor;
	}
	
	public static SelectionHelper of(ITextEditor textEditor) {
		return new SelectionHelper(textEditor);
	}
	
	public Region currentSelection() {
		ISelectionProvider selectionProvider = textEditor.getSelectionProvider();
		ISelection selection = selectionProvider.getSelection();
		ITextSelection textSelection = (ITextSelection) selection;
		int offset = textSelection.getOffset();
		int length = textSelection.getLength();

		return new Region(offset, length);
	}

	public boolean allMatch(Region selection, Predicate<Character> predicate) {
		return selection.stream().mapToObj(this::charAt).allMatch(predicate);
	}
	
	public char charAt(int offset) {
		try {
			IDocument document = getDocument();
			return document.getChar(offset);
		} catch (BadLocationException e) {
			throw new IllegalArgumentException("Invalid offset: %d".formatted(offset), e);
		}
	}
	
	public String getText(Region region) {
		return region.stream().mapToObj(this::charAt).collect(Collector.of(
			    StringBuilder::new,
			    StringBuilder::append,
			    StringBuilder::append,
			    StringBuilder::toString));
	}

	public IDocument getDocument() {
		IDocumentProvider documentProvider = textEditor.getDocumentProvider();
		IEditorInput editorInput = textEditor.getEditorInput();
		IDocument document = documentProvider.getDocument(editorInput);
		return document;
	}

	public Region expand(Region region, Predicate<Character> predicate) {
		int offset = region.offset();
		int length = region.length();
		
		while (offset > 0 && predicate.test(charAt(offset - 1))) {
			offset--;
			length++;
		}
		
		IDocument document = getDocument();
		while (offset + length < document.getLength()
				&& predicate.test(charAt(offset + length))) {
			length++;
		}
		
		return new Region(offset, length);
	}

	public void replace(Region region, String replacement, Runnable andThen) {
		try {
			String text = getText(region);
			if(text.equals(replacement))return;
			
			getDocument().replace(region.offset(), region.length(), replacement);
			andThen.run();
		} catch (BadLocationException e) {
			throw new IllegalArgumentException("Invalid region: %d".formatted(region), e);
		}		
	}
	
	

}
