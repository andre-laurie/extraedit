package com.alaurie.text;

import java.lang.invoke.MethodHandles;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.logging.Logger;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.texteditor.ITextEditor;

public class SelectCurrentIdentifierHandler extends AbstractHandler {

	private Logger logger = Logger.getLogger(MethodHandles.lookup().getClass().getName());

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IEditorPart activeEditorChecked = HandlerUtil.getActiveEditorChecked(event);
		IWorkbenchPart activePart = HandlerUtil.getActivePart(event);
		logger.info("SelectCurrentIdentifierHandler.execute(): %s".formatted(activePart));

		if (activeEditorChecked instanceof ITextEditor textEditor) {

			SelectionHelper selectionHelper = SelectionHelper.of(textEditor);

			Optional<Region> expanded = selectCurrentIdentifier(selectionHelper);
			expanded.ifPresent(region -> textEditor.selectAndReveal(region.offset(), region.length()));
		}

		return null;
	}

	private Optional<Region> selectCurrentIdentifier(SelectionHelper selectionHelper) {
		Predicate<Character> predicate = Character::isJavaIdentifierPart;

		Region selection = selectionHelper.currentSelection();
		if (!selectionHelper.allMatch(selection, predicate)) {
			logger.info("Incompatible char in current selection, aborting");
			return Optional.empty();
		}

		Region expanded = selectionHelper.expand(selection, predicate);
		if (expanded.equals(selection))
			return Optional.empty();

		return Optional.of(expanded);
	}
}