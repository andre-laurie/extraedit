package com.alaurie.text;

import java.util.Optional;
import java.util.function.Predicate;

import org.apache.commons.text.WordUtils;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.texteditor.ITextEditor;

public class TitleCaseHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IEditorPart activeEditorChecked = HandlerUtil.getActiveEditorChecked(event);

		if (activeEditorChecked instanceof ITextEditor textEditor) {
			SelectionHelper selectionHelper = SelectionHelper.of(textEditor);

			Region selection = selectionHelper.currentSelection();

			Predicate<Character> predicate = Character::isJavaIdentifierPart;
			
			Region workingRegion = Optional.of(selection).filter(Region::isEmpty)
					.map(region -> selectionHelper.expand(region, predicate)).orElse(selection);

			String text = selectionHelper.getText(workingRegion);
			Runnable runnable;
			if (selection.isEmpty()) {
				Control beforeFocusControl = Display.getCurrent().getFocusControl();
				if (beforeFocusControl instanceof StyledText styledText) {
					int caretOffset = styledText.getCaretOffset();
					runnable = () -> styledText.setCaretOffset(caretOffset);
				} else
					runnable = () -> {
					};
			} else {
				runnable = () -> textEditor.getSelectionProvider()
						.setSelection(new TextSelection(selection.offset(), selection.length()));
			}

			String titleCase = WordUtils.capitalizeFully(text);
			selectionHelper.replace(workingRegion, titleCase, runnable);
		}

		return null;
	}
}